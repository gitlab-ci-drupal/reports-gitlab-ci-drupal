FROM node:lts-alpine

RUN mkdir -p ./dashboard
COPY . /dashboard

RUN apk --no-cache add bash jq tree curl \
  && cd /dashboard \
  && yarn install \
  && yarn build

WORKDIR /dashboard
